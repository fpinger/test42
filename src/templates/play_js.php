<script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
<script>
    const MyComponentActions = {
        props: ['actions',],
        template: `
            <div class="col-10">
                Для решения загадки вам нужно выбрать правильный порядок действий:
                <ul class="list-group">
                    <li v-for="action in actions" :key="'action-' + action.id" class="list-group-item">
                        <a href="#" v-on:click.prevent="onClick(action.id)">{{ action.message }}</a>
                    </li>
                </ul>
            </div>
            `,
        methods: {
            onClick (passengerId) {
                this.$emit('actionSelected', {passengerId: passengerId})
            },
        },
    }
    
    const MyComponentPassengers = {
        props: ['passengers',],
        data() {
            return { 
                passengersNames: {
                    wolf: "Волк",
                    goat: "Коза",
                    cabbage: "Капуста",
                }
            }
        },
        template: `
            <div class="row">
                <div class="col">
                    <div class="card-group">
                        <div v-for="passenger in passengers" :key="'passenger-' + passenger" class="card" style="min-width: 110px;max-width: 110px;">
                            <img :src="'/img/' + passenger + '.png'" class="card-img-top" :alt="passenger">
                            <div class="card-body">
                                <p class="card-text">{{ passengersNames[passenger] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`,
    }

    const MyComponentLocation = {
        components: {
            'my-component-passengers': MyComponentPassengers,
        },
        props: ['title', 'passengers'],
        template: `
            <div class="row">
                <div class="col" style="background:#eee;">
                    {{ title }}
                    <my-component-passengers :passengers="passengers"></my-component-passengers>
                </div>
            </div>`,
    }

    const app = Vue.createApp({
        data() {
            return {
                state: null,
            }
        },
        components: {
            'my-component-actions': MyComponentActions,
            'my-component-location': MyComponentLocation,
        },
        created () {
            fetch('/play/api/init')
                .then((response) => {
                    return response.json()
                })
                .then((data) => {
                    this.state = data;
                    console.log("State", this.state);
                });
        },
        methods: {
            onActionSelected (data) {
                console.log("passengerId!!!", data.passengerId);

                fetch('/play/api/action', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8'
                    },
                    body: JSON.stringify(data)
                }).then((response) => {
                    return response.json()
                }).then((data) => {
                    this.state = data;
                    console.log("New state", this.state);
                });
            },
        },
    });
    app.config.productionTip = false; // Разработка
    app.mount('#play');
</script>
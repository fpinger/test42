<div id="play" v-cloak>
    <div class="row">
        <div class="col">
            <h1>Поиск решения загадки</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            Есть загадка: "Крестьянину нужно перевезти через реку волка, козу и капусту. 
            Но лодка такова, что в ней может поместиться только крестьянин, а с ним или один волк, или одна коза, или одна капуста. 
            Но если оставить волка с козой, то волк съест козу, а если оставить козу с капустой, то коза съест капусту. 
            Как перевез свой груз крестьянин?"
        </div>
    </div>

    <div class="row">
        <div class="col-2">
            <div class="card" style="width: 150px;">
                <img src="/img/peasant.jpg" class="card-img-top" alt="peasant">
                <div class="card-body">
                    <p class="card-text"><?=htmlspecialchars($pseudonym)?></p>
                </div>
            </div>
        </div>

        <div class="col-10" v-if="state && (state['result'] == 'defeat' || state['result'] == 'success')">
            <div v-if="state['result'] == 'success'" class="alert alert-success" role="alert">
                Вы решили!
            </div>
    
            <div v-if="state['result'] == 'defeat'" class="alert alert-danger" role="alert">
                Вы ошиблись! {{ state['conflict_result'] }}...
            </div>

            <form action="/clear-session" method="post">
                <button type="submit" class="btn btn-primary">Попробовать ещё раз или посмотреть список разгадавших</button>
            </form>
        </div>

        <my-component-actions
            v-if="state && state['result'] == 'progress'"
            v-bind:actions="state['actions']"
            v-on:action-selected="onActionSelected"
        ></my-component-actions>

    </div>

    <my-component-location 
        title="Берег откуда везём."
        v-if="state"
        v-bind:passengers="state['locations']['from']"
    ></my-component-location>

    <div class="row">
        <div class="col" style="background:blue;color:white;">
            Река.
        </div>
    </div>
    
    <my-component-location 
        title="Берег куда везём." 
        v-if="state"
        v-bind:passengers="state['locations']['to']"
    ></my-component-location>
    
</div>



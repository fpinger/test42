<div class="row">
    <div class="col">
        <h1>Регистрация для решения загадки</h1>

        <p>
            Что бы зафиксировать результат нам нужно любое выбранное вами уникальное имя.
            Регистр букв не имеет значения.
            Часть имен может быть уже занята.
        </p>

    <?php if (isset($reg_error)): ?>
        <div class="alert alert-danger" role="alert">
            <?=$reg_error?>
        </div>
    <?php endif; ?>

        <form action="/create-player" method="post">
            <div class="mb-3">
                <label for="pseudonym" class="form-label">Уникальное имя:</label>
                <input type="text" class="form-control" id="pseudonym" name="pseudonym" aria-describedby="pseudonymHelp">
                <div id="pseudonymHelp" class="form-text">Если имя занято, то выбирите любое другое.</div>
            </div>
            <button type="submit" class="btn btn-primary">Перейти к загадке</button>
        </form>

    <?php if ($players): ?>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Псевдоним</th>
                    <th scope="col">Окончил</th>
                    <th scope="col">Результат</th>
                </tr>
            </thead>
            <tbody>
        <?php foreach($players as $player):?>
                <tr>
                    <th scope="row"><?=$player["id"]?></th>
                    <td><?=htmlspecialchars($player["pseudonym"])?></td>
                    <td><?=$player["updated_at"]?></td>
                    <td>
                    <?php if ($player["solved_state"] == RESULT_SUCCESS): ?>
                        <div class="alert alert-success">Разгадано</div>                    
                    <?php elseif ($player["solved_state"] == RESULT_DEFEAT): ?>
                        <div class="alert alert-danger">Не разгадано</div>                    
                    <?php else: ?>
                        <div class="alert alert-secondary">Еще решает</div>                    
                    <?php endif; ?>
                    </td>
                </tr>
        <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
    </div>
</div>

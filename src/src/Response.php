<?php

namespace Fpinger\Test24;

class Response
{
    const RESPONSE_TYPE_TXT = 'txt';
    const RESPONSE_TYPE_HTML = 'html';
    const RESPONSE_TYPE_REDIRECT = 'redirect';
    const RESPONSE_TYPE_JSON = 'json';
    
    private $type;
    private $content;
    private $code;

    public function __construct()
    {
        $this->type = self::RESPONSE_TYPE_HTML;
    }

    public function text($text, $code=200)
    {
        $this->type = self::RESPONSE_TYPE_TXT;
        $this->content = $text;
        $this->code = $code;
        return $this;
    }

    public function html($html, $code=200)
    {
        $this->type = self::RESPONSE_TYPE_HTML;
        $this->content = $html;
        $this->code = $code;
        return $this;
    }

    public function redirect($url, $code=302)
    {
        $this->type = self::RESPONSE_TYPE_REDIRECT;
        $this->content = $url;
        $this->code = $code;
        return $this;
    }

    public function json($array, $code=200)
    {
        $this->type = self::RESPONSE_TYPE_JSON;
        $this->content = $array;
        $this->code = $code;
        return $this;
    }

    public function render()
    {
        if ($this->code) {
            http_response_code($this->code);
        }
        // --
        switch ($this->type) {
            case self::RESPONSE_TYPE_TXT:
                echo $this->content;
                break;
            case self::RESPONSE_TYPE_HTML:
                echo $this->content;
                break;
            case self::RESPONSE_TYPE_JSON:
                echo json_encode($this->content);
                break;
            case self::RESPONSE_TYPE_REDIRECT:
                $code = in_array($this->code, [301, 302]) ? $this->code : 302;
                header("Location: {$this->content}", true, $code);
                break;
        }
        exit();
    }
}

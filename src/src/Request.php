<?php

namespace Fpinger\Test24;

class Request
{
    const METHOD_TYPE_GET = 'GET';
    const METHOD_TYPE_POST = 'POST';

    private $url;
    private $path;
    private $method;
    private $query;
    private $data;
    private $parameters;

    public function __construct($serverInfo, $queryInfo, $postData)
    {
        $this->init($serverInfo, $queryInfo, $postData);
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function isMethod($methodType)
    {
        return ($this->getMethod() === strtoupper($methodType));
    }

    public function setParams($parameters)
    {
        $this->parameters = $parameters;
    }

    public function getParams()
    {
        return $this->parameters;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getDataValue($key, $default=null)
    {
        $data = $this->getData();
        if (!isset($data[$key])) {
            return $default;
        }
        $value = trim($data[$key]); // Так не нужно, но все же
        if (empty($value)) {
            return $default;
        }
        return $value;
    }

    public function getJson()
    {
        return file_get_contents('php://input');
    }

    public function getUrl()
    {
        return $this->url;
    }

    private function init($serverInfo, $queryInfo, $postData)
    {
        $this->url = $serverInfo['REQUEST_URI'];
        $this->path = $this->initRequestPath($this->url);
        $this->method = $serverInfo['REQUEST_METHOD'];
        $this->query = $queryInfo;
        $this->data = $postData;
    }

    private function initRequestPath($requsetUri) 
    {
        $raw_path = explode('?', $requsetUri)[0];
        $raw_path = trim($raw_path, '/');
        return empty($raw_path) ? '/' : $raw_path;
    }
}

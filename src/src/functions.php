<?php

use Dotenv\Dotenv;

function loadEnv($path)
{
    $dotenv = Dotenv::createImmutable($path);
    $dotenv->load();
}

function getDbTimezone($tz)
{
    return (new DateTime("now", new DateTimeZone($tz)))->format("P");
}

function getDbConnection($env, $offset)
{
    $dsn = "mysql:host={$env['MYSQL_HOST']};dbname={$env['MYSQL_DATABASE']};charset=utf8mb4";
    $opt = [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false,
        \PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone='{$offset}'"
    ];
    $pdo = new \PDO($dsn, $env["MYSQL_USER"], $env["MYSQL_PASSWORD"], $opt);
    //$pdo->exec("SET time_zone='{$offset}';");
    return $pdo;
}

function findByPseudonym($pdo, $pseudonym)
{
    $sql = "SELECT * FROM players WHERE pseudonym = :pseudonym LIMIT 1";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['pseudonym' => $pseudonym]);
    return $stmt->fetch();
}

function createByPseudonym($db, $pseudonym, $solvedState)
{
    $sql = "INSERT INTO players (pseudonym, solved_state, created_at, updated_at)" .
        " VALUES (:pseudonym, :solved_state, NOW(), NOW())";
    $stm = $db->prepare($sql);
    $stm->execute([
        "pseudonym" => $pseudonym,
        "solved_state" => $solvedState, 
    ]);
    return $db->lastInsertId();
}

function updateByUserId($db, $id, $solvedState)
{
    $sql = "UPDATE players SET solved_state = :solved_state WHERE id = :id";
    $stm = $db->prepare($sql);
    $stm->execute([
        "id" => $id,
        "solved_state" => $solvedState, 
    ]);
}

function initDb ($pdo)
{
    $sql = "CREATE TABLE IF NOT EXISTS `players` (" .
        " `id` int(11) NOT NULL AUTO_INCREMENT," .
        " `pseudonym` varchar(255) NOT NULL," .
        " `solved_state` varchar(32) NOT NULL," .
        " `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'," .
        " `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()," .
        " PRIMARY KEY (`id`)" .
        " ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;";
    $stmt = $pdo->query($sql);
}

function getTenLastPlayers ($pdo)
{
    $sql = "SELECT * FROM `players` ORDER BY created_at DESC LIMIT 10";
    $stmt = $pdo->query($sql);
    return $stmt->fetchAll();
}

// ---------------------------------------------------------------------------
define("DEFAULT_LOCATION", "from");   // От сюда перевозим

define("NO_PASSENGER", "nothing"); // Ничкого и ничего не перевозим
define("PASSENGERS", ["wolf", "goat", "cabbage"]); // То что перевозим
define("PASSENGERS_INSTRUMENTAL", ["wolf" => "волком", "goat" => "козой", "cabbage" => "капустой"]); // То что перевозим в тварительном падеже

define("RESULT_PROGRESS", "progress"); // Решение в процессе
define("RESULT_SUCCESS", "success");   // Загадка решена
define("RESULT_DEFEAT", "defeat");     // Поражение

define("CONFLICTS", [["wolf", "goat", "Волк съел козу"], ["goat", "cabbage", "Коза сьела капусту"],]); // Конфликты
define("PARTICIPANT_IN_CONFLICT_1", 0);
define("PARTICIPANT_IN_CONFLICT_2", 1);
define("CONFLICT_RESULT", 2);

/*
* Инициализация загадки
*/
function initPlayState ()
{
    return [
        "locations" => [
            "from" => PASSENGERS,
            "to" => [],
        ],
        "currentLocation" => DEFAULT_LOCATION,
        "actions" => [],
        "result"  => RESULT_PROGRESS,
    ];
}

/*
* Проверка правильности перевозимого
*/
function isPassengerValid ($passenger)
{
    return ($passenger === NO_PASSENGER || in_array($passenger, PASSENGERS));
}

function getActionPrefix ($currentLocation)
{
    return ($currentLocation === "from" ? "Переплыть" : "Вернуться");
}

function getOtherLocation ($location) 
{
    return $location === "from" ? "to": "from";
}

function setActions($state)
{
    $prefix = getActionPrefix($state["currentLocation"]);
    $state["actions"][] = [
        "id"      => NO_PASSENGER,
        "message" => "{$prefix} пустым",
    ];
    foreach ($state["locations"][$state["currentLocation"]] as $passenger) {
        $word = PASSENGERS_INSTRUMENTAL[$passenger];
        $state["actions"][] = [
            "id"      => $passenger,
            "message" => "{$prefix} с {$word}",
        ];
    }
    return $state;
}

function updateState($state, $newLocation, $passenger)
{
    if ($passenger !== NO_PASSENGER) {
        $state = removePassangerFromLoaction($state, $state["currentLocation"], $passenger);
    }
    $state["currentLocation"] = $newLocation;
    if ($passenger !== NO_PASSENGER) {
        $state["locations"][$state["currentLocation"]][] = $passenger;
    }
    return $state;
}

function removePassangerFromLoaction($state, $location, $passenger)
{
    if (($key = array_search($passenger, $state["locations"][$location])) !== false) {
        unset($state["locations"][$location][$key]);
    }
    return $state;
}

function isMysterySolved($state)
{
    return empty($state["locations"][DEFAULT_LOCATION]);
}

function checkConflict($state)
{
    $location = getOtherLocation($state["currentLocation"]); // Противоположный берег
    $items = $state["locations"][$location];
    foreach (CONFLICTS as $conflict) {
        if (in_array($conflict[PARTICIPANT_IN_CONFLICT_1], $items) 
            && in_array($conflict[PARTICIPANT_IN_CONFLICT_2], $items)) {

            return $conflict[CONFLICT_RESULT];
        }
    }
    return "";
}
<?php

namespace Fpinger\Test24;

class Session
{
    public function __construct()
    {
        session_start();
    }

    public function valueExists($name)
    {
        return isset($_SESSION[$name]);
    }

    public function setValue($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public function getValue($name, $default=null)
    {
        return $this->valueExists($name) ? $_SESSION[$name]: $default;
    }

    public function removeValue($name)
    {
        if ($this->valueExists($name)) {
            unset($_SESSION[$name]);
        }
    }

    public function flushValue($name, $default=null)
    {
        $value = $this->getValue($name, $default);
        $this->removeValue($name);
        return $value;
    }

    // todo: Зачистка сессии.
}
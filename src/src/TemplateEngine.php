<?php

namespace Fpinger\Test24;

class TemplateEngine
{
    private $template_path;

    public function __construct($templatePath)
    {
        $this->template_path = $templatePath;
    }

    public function render($templateName, $valuesArray=[])
    {
        $template = $this->template($templateName);

        ob_start();
        
        extract($valuesArray);
        require "{$template}";

        $content = ob_get_clean();
        return $content;
    }

    private function template($templateName) {
        return "{$this->template_path}/{$templateName}";
    }

}
<?php

require_once __DIR__ . '/vendor/autoload.php';

use Fpinger\Test24\Request;
use Fpinger\Test24\Response;
use Fpinger\Test24\Session;
use Fpinger\Test24\TemplateEngine;

define("TEMPLATE_PATH", __DIR__ . "/templates");
define("ENV_PATH", __DIR__ . '/../');
define("SESSION_PLAYER_ID", "player_id");
define("SESSION_PLAYER_NAME", "player");
define("SESSION_REG_ERROR", "reg_error");
define("SESSION_PLAY_STATE", "play_state");
define("TIMEZONE", "Asia/Vladivostok");

date_default_timezone_set(TIMEZONE);
// ---

loadEnv(ENV_PATH); // Загружаем переменные окружения из файла .env

$session = new Session();
$request = new Request($_SERVER, $_GET, $_POST);
$response = new Response();

$template_engine = new TemplateEngine(TEMPLATE_PATH);

$tz = getDbTimezone(TIMEZONE);
$db = getDbConnection($_ENV, $tz);
initDb($db);

// ---
switch ($request->getPath()) {
    case '/': // -------------------------- Страница регистрации пользователя
        $player = $session->getValue(SESSION_PLAYER_NAME);
        if ($player) { // Редирект на игру
            $response->redirect("/play")->render();
        }
        $content = $template_engine->render("layout.php", [
            "title"     => "Регистрация",
            "content"   => $template_engine->render("auth.php", [
                "reg_error" => $session->flushValue(SESSION_REG_ERROR),
                "players"   => getTenLastPlayers($db),
            ]),
        ]);
        $response->html($content)->render();
        break;
    case 'create-player': // -------------- Регистрируем пользователя
        $player = $session->getValue(SESSION_PLAYER_NAME);
        if ($player) { // Редирект на игру
            $response->redirect("/play")->render();
        }
        
        $pseudonym = $request->getDataValue("pseudonym");
        if (!$pseudonym) {
            $session->setValue(SESSION_REG_ERROR, "Не указано имя.");
            $response->redirect("/")->render();
        }

        $user = findByPseudonym($db, $pseudonym);
        if ($user) {
            $session->setValue(SESSION_REG_ERROR, "Имя уже занято.");
            $response->redirect("/")->render();
        }

        $solved_state = RESULT_PROGRESS; // Состояние разгадки
        $id = createByPseudonym($db, $pseudonym, $solved_state);

        $session->setValue(SESSION_PLAYER_ID, $id);
        $session->setValue(SESSION_PLAYER_NAME, $pseudonym);

        $response->redirect("/play")->render();
        break;
    case 'play': // ----------------------- Страница загадки
        $player = $session->getValue(SESSION_PLAYER_NAME);
        if (!$player) { // Редирект на регистрацию
            $response->redirect("/")->render();
        } else if (!$session->valueExists(SESSION_PLAY_STATE)) {
            $session->setValue(SESSION_PLAY_STATE, initPlayState());
        }

        $pseudonym = $session->getValue(SESSION_PLAYER_NAME);

        $content = $template_engine->render("layout.php", [
            "title"   => "Разгадываем",
            "content" => $template_engine->render("play.php", ["pseudonym" => $pseudonym]),
            "js"      => $template_engine->render("play_js.php", []),
        ]);
        $response->html($content)->render();
        break;
    case 'play/api/init': // -------------- Инициализируем загадку
        $state = $session->getValue(SESSION_PLAY_STATE);
        if ($state["result"] === RESULT_PROGRESS) {
            $state = setActions($state);
        }
        $response->json($state)->render();
        break;
    case 'play/api/action': // ------------ Проверяем действия пользователя
        $data = json_decode($request->getJson(), true);
        $passenger = isset($data["passengerId"]) ? $data["passengerId"]: "";
        if (!isset($passenger) || !isPassengerValid($passenger)) {
            $response->json(["error" => "Passenger \"{$passenger}\" not found"])->render();
        }

        $state = $session->getValue(SESSION_PLAY_STATE);
        if ($passenger !== NO_PASSENGER && !in_array($passenger, $state["locations"][$state["currentLocation"]])) {
            $response->json(["error" => "Not valid passenger \"{$passenger}\" in current location"])->render();
        }
        $new_location = getOtherLocation($state["currentLocation"]);
        $state = updateState($state, $new_location, $passenger);
        if (isMysterySolved($state)) {
            $state["result"]  = RESULT_SUCCESS;
            
            $id = $session->getValue(SESSION_PLAYER_ID);
            updateByUserId($db, $id, RESULT_SUCCESS);

            $session->setValue(SESSION_PLAY_STATE, $state);
        } else if ($conflict_result = checkConflict($state)) {
            $state["result"]  = RESULT_DEFEAT;
            $state["conflict_result"]  = $conflict_result;

            $id = $session->getValue(SESSION_PLAYER_ID);
            updateByUserId($db, $id, RESULT_DEFEAT);

            $session->setValue(SESSION_PLAY_STATE, $state);
        } else {
            $session->setValue(SESSION_PLAY_STATE, $state);
            $state = setActions($state);
        }
        $response->json($state)->render();
        break;
    case 'clear-session': // -------------- Сбрасываем сессию для новой попытки
        $player = $session->getValue(SESSION_PLAYER_NAME);
        if (!$player) { // Редирект на регистрацию
            $response->redirect("/")->render();
        }
        $session->removeValue(SESSION_PLAYER_ID);
        $session->removeValue(SESSION_PLAYER_NAME);
        $session->removeValue(SESSION_PLAY_STATE);
        $response->redirect("/")->render();
        break;
    default: // --------------------------- Страница не найдена
        $content = $template_engine->render("layout.php", [
            "title"   => "Ошибка 404. Страница не найдена",
            "content" => "<h1>Страница не найдена</h1>", // todo: Вынести в шаблон
        ]);
        $response->html($content, 404)->render();
}